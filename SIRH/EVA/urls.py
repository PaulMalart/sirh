from django.urls import path, include
from .views import *

urlpatterns = [

    path('autorisations/', autorisations, name="autorisations"),
    path('autorisations/new/', newAutorisation, name="newAutorisation"),
    path('etatdetravaux/', etatDeTravaux, name="etatDeTravaux"),
    path('planning/', planning, name="planning"),
    path('planning/addcours/', newCours, name="newCours"),
    path('vacataires/', vacataires, name="vacataires"),
    path('vacataires/new/', newVacataire, name="newVacataire"),
    path('vacataires/show/', detailsVacataire, name="detailsVacataire"),
]